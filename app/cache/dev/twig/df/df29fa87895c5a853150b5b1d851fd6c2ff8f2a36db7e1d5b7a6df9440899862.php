<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_e85e58342406dd9a2b17a4118c4a13c97a837d665cc5ff291fe436101887816f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e5590af2713e2ad470b5781ee0041c1490f221ff4891e299c3b5d26da1006c68 = $this->env->getExtension("native_profiler");
        $__internal_e5590af2713e2ad470b5781ee0041c1490f221ff4891e299c3b5d26da1006c68->enter($__internal_e5590af2713e2ad470b5781ee0041c1490f221ff4891e299c3b5d26da1006c68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_e5590af2713e2ad470b5781ee0041c1490f221ff4891e299c3b5d26da1006c68->leave($__internal_e5590af2713e2ad470b5781ee0041c1490f221ff4891e299c3b5d26da1006c68_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
