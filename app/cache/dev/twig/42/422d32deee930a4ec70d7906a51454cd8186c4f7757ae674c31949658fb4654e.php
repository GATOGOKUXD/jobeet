<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_b5cff0ea4e7600c18e71f90fc7e348e7fbb591bea771ffa1158774971e27f3bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5e4e057a32b118fdf7f4e03455e6f8c6dbff40a2f2bbf5795087f7f82bb8cefd = $this->env->getExtension("native_profiler");
        $__internal_5e4e057a32b118fdf7f4e03455e6f8c6dbff40a2f2bbf5795087f7f82bb8cefd->enter($__internal_5e4e057a32b118fdf7f4e03455e6f8c6dbff40a2f2bbf5795087f7f82bb8cefd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_5e4e057a32b118fdf7f4e03455e6f8c6dbff40a2f2bbf5795087f7f82bb8cefd->leave($__internal_5e4e057a32b118fdf7f4e03455e6f8c6dbff40a2f2bbf5795087f7f82bb8cefd_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
