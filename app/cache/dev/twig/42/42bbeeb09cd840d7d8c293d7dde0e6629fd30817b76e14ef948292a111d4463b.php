<?php

/* @Twig/Exception/exception.json.twig */
class __TwigTemplate_ba072291f5af193a11fc8b7332b49a58c3b20f78a2c933b863d3472d0790b348 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a0f0b03f6f68620a1298c493c83c295fc51a0a76a1f7451354018345a9d7f42 = $this->env->getExtension("native_profiler");
        $__internal_7a0f0b03f6f68620a1298c493c83c295fc51a0a76a1f7451354018345a9d7f42->enter($__internal_7a0f0b03f6f68620a1298c493c83c295fc51a0a76a1f7451354018345a9d7f42_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_7a0f0b03f6f68620a1298c493c83c295fc51a0a76a1f7451354018345a9d7f42->leave($__internal_7a0f0b03f6f68620a1298c493c83c295fc51a0a76a1f7451354018345a9d7f42_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
