<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_71882eca5035adedfcf44aa5402530574e0e0d9dac4e13e2646d22a74c4d44c7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_689ec7d840a820fd5f6fee471cac4f2e892ce9db34c353b26e8b5fcbe863d83b = $this->env->getExtension("native_profiler");
        $__internal_689ec7d840a820fd5f6fee471cac4f2e892ce9db34c353b26e8b5fcbe863d83b->enter($__internal_689ec7d840a820fd5f6fee471cac4f2e892ce9db34c353b26e8b5fcbe863d83b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_689ec7d840a820fd5f6fee471cac4f2e892ce9db34c353b26e8b5fcbe863d83b->leave($__internal_689ec7d840a820fd5f6fee471cac4f2e892ce9db34c353b26e8b5fcbe863d83b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'submit')) ?>*/
/* */
