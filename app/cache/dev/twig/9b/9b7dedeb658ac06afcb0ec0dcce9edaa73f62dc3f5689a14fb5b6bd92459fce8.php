<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_5b1e4743730f22c7a3b8dd879084baf6a475b29be2456809b26aef6505629ee8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0fac0455cd6a77451c21290aaf50e89979996b6d6aeaf192af92044fa4d682b3 = $this->env->getExtension("native_profiler");
        $__internal_0fac0455cd6a77451c21290aaf50e89979996b6d6aeaf192af92044fa4d682b3->enter($__internal_0fac0455cd6a77451c21290aaf50e89979996b6d6aeaf192af92044fa4d682b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_0fac0455cd6a77451c21290aaf50e89979996b6d6aeaf192af92044fa4d682b3->leave($__internal_0fac0455cd6a77451c21290aaf50e89979996b6d6aeaf192af92044fa4d682b3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <textarea <?php echo $view['form']->block($form, 'widget_attributes') ?>><?php echo $view->escape($value) ?></textarea>*/
/* */
