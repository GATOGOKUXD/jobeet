<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_9629aef5c6141ddaaf971237a2e679af072e0e05a8a52d804831487e9225d2f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58c9f768e6fd6897d1fbb40e4756a44210ed77f736e86439238116e9fd0ba434 = $this->env->getExtension("native_profiler");
        $__internal_58c9f768e6fd6897d1fbb40e4756a44210ed77f736e86439238116e9fd0ba434->enter($__internal_58c9f768e6fd6897d1fbb40e4756a44210ed77f736e86439238116e9fd0ba434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_58c9f768e6fd6897d1fbb40e4756a44210ed77f736e86439238116e9fd0ba434->leave($__internal_58c9f768e6fd6897d1fbb40e4756a44210ed77f736e86439238116e9fd0ba434_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($compound): ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_compound')?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'form_widget_simple')?>*/
/* <?php endif ?>*/
/* */
