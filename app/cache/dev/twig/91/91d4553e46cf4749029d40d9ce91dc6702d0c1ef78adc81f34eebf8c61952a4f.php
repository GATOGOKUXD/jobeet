<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_5d0b8a1f449fadaa20ed875ce5651c4a8a7e6064e1fbb6805bac041cc121652c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0002fcca8fd0d1d73d972d5927a8f5f47fb3aaba6c00a7457582bc4c9d60d29b = $this->env->getExtension("native_profiler");
        $__internal_0002fcca8fd0d1d73d972d5927a8f5f47fb3aaba6c00a7457582bc4c9d60d29b->enter($__internal_0002fcca8fd0d1d73d972d5927a8f5f47fb3aaba6c00a7457582bc4c9d60d29b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_0002fcca8fd0d1d73d972d5927a8f5f47fb3aaba6c00a7457582bc4c9d60d29b->leave($__internal_0002fcca8fd0d1d73d972d5927a8f5f47fb3aaba6c00a7457582bc4c9d60d29b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (isset($prototype)): ?>*/
/*     <?php $attr['data-prototype'] = $view->escape($view['form']->row($prototype)) ?>*/
/* <?php endif ?>*/
/* <?php echo $view['form']->widget($form, array('attr' => $attr)) ?>*/
/* */
