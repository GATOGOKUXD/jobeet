<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_ea2476767aba41c0d046fd11badcd74005133d06ab664427e65280475f62e867 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_106f7a9355bd8c6b49ee26cdcee29c57b603eb35e108528479bc3bad0ad8c85e = $this->env->getExtension("native_profiler");
        $__internal_106f7a9355bd8c6b49ee26cdcee29c57b603eb35e108528479bc3bad0ad8c85e->enter($__internal_106f7a9355bd8c6b49ee26cdcee29c57b603eb35e108528479bc3bad0ad8c85e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_106f7a9355bd8c6b49ee26cdcee29c57b603eb35e108528479bc3bad0ad8c85e->leave($__internal_106f7a9355bd8c6b49ee26cdcee29c57b603eb35e108528479bc3bad0ad8c85e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($expanded): ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_expanded') ?>*/
/* <?php else: ?>*/
/* <?php echo $view['form']->block($form, 'choice_widget_collapsed') ?>*/
/* <?php endif ?>*/
/* */
