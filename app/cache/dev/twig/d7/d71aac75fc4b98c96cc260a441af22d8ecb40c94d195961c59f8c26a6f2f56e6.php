<?php

/* job/show.html.twig */
class __TwigTemplate_29cbcc2dad70103b86967b90baa99957e3aabc0da47786d34de0113ce4acfef2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "job/show.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cddd980d1b5988cb8b35627929144c60a8ff41cad323b3b99d6d481f685028e = $this->env->getExtension("native_profiler");
        $__internal_0cddd980d1b5988cb8b35627929144c60a8ff41cad323b3b99d6d481f685028e->enter($__internal_0cddd980d1b5988cb8b35627929144c60a8ff41cad323b3b99d6d481f685028e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "job/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0cddd980d1b5988cb8b35627929144c60a8ff41cad323b3b99d6d481f685028e->leave($__internal_0cddd980d1b5988cb8b35627929144c60a8ff41cad323b3b99d6d481f685028e_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_440503223e02d52fe67ddb903ea6029569fb556d47a97f6288a13ab6b44af315 = $this->env->getExtension("native_profiler");
        $__internal_440503223e02d52fe67ddb903ea6029569fb556d47a97f6288a13ab6b44af315->enter($__internal_440503223e02d52fe67ddb903ea6029569fb556d47a97f6288a13ab6b44af315_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<div id=\"content\" class=\"jobs\">
 <table>
 <thead>
 <tr>
 <th hidden=\"true\" >Localización</th>
 <th hidden=\"true\" >Cargo</th>
 <th hidden=\"true\" >Empresa</th>
 <th hidden=\"true\" >Fecha creación</th>
 </tr>
 </thead>
 <tbody>
 <tr>
 <td class=\"location\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "location", array()), "html", null, true);
        echo "</td>
 <td class=\"position\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "position", array()), "html", null, true);
        echo "</td>
 <td class=\"company\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "company", array()), "html", null, true);
        echo "</td>

 <td hidden=\"true\" >
 <small>publicado ";
        // line 20
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "expiresAt", array()), "m/d/Y"), "html", null, true);
        echo "</small>
 </td>
 <td>
 <div hidden=\"true\" style=\"padding: 20px 0\">
 <ul>
 <li>
 <a href=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("job_show", array("id" => $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "id", array()))), "html", null, true);
        // line 27
        echo "\">mostrar</a>
 </li>
 <li>
 <a href=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("job_edit", array("id" => $this->getAttribute((isset($context["job"]) ? $context["job"] : $this->getContext($context, "job")), "id", array()))), "html", null, true);
        // line 31
        echo "\">editar</a>
 </li>
 </ul>
 </div>
 </td>
 </tr>
 </tbody>
 </table>

</div>
";
        
        $__internal_440503223e02d52fe67ddb903ea6029569fb556d47a97f6288a13ab6b44af315->leave($__internal_440503223e02d52fe67ddb903ea6029569fb556d47a97f6288a13ab6b44af315_prof);

    }

    public function getTemplateName()
    {
        return "job/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 31,  84 => 30,  79 => 27,  77 => 26,  68 => 20,  62 => 17,  58 => 16,  54 => 15,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content -%}*/
/* <div id="content" class="jobs">*/
/*  <table>*/
/*  <thead>*/
/*  <tr>*/
/*  <th hidden="true" >Localización</th>*/
/*  <th hidden="true" >Cargo</th>*/
/*  <th hidden="true" >Empresa</th>*/
/*  <th hidden="true" >Fecha creación</th>*/
/*  </tr>*/
/*  </thead>*/
/*  <tbody>*/
/*  <tr>*/
/*  <td class="location">{{ job.location }}</td>*/
/*  <td class="position">{{ job.position }}</td>*/
/*  <td class="company">{{ job.company }}</td>*/
/* */
/*  <td hidden="true" >*/
/*  <small>publicado {{ job.expiresAt|date("m/d/Y") }}</small>*/
/*  </td>*/
/*  <td>*/
/*  <div hidden="true" style="padding: 20px 0">*/
/*  <ul>*/
/*  <li>*/
/*  <a href="{{ path('job_show', { 'id': job.id })*/
/* }}">mostrar</a>*/
/*  </li>*/
/*  <li>*/
/*  <a href="{{ path('job_edit', { 'id': job.id })*/
/* }}">editar</a>*/
/*  </li>*/
/*  </ul>*/
/*  </div>*/
/*  </td>*/
/*  </tr>*/
/*  </tbody>*/
/*  </table>*/
/* */
/* </div>*/
/* {% endblock %} */
