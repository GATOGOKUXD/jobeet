<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_95a7fa05724527beb777ee857214984bf81e3906f3f8e830429dab997d246387 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_459aa28be2c9e7f8465503ef47533d66d0c45c65e20861b803a7b17a728eab96 = $this->env->getExtension("native_profiler");
        $__internal_459aa28be2c9e7f8465503ef47533d66d0c45c65e20861b803a7b17a728eab96->enter($__internal_459aa28be2c9e7f8465503ef47533d66d0c45c65e20861b803a7b17a728eab96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_459aa28be2c9e7f8465503ef47533d66d0c45c65e20861b803a7b17a728eab96->leave($__internal_459aa28be2c9e7f8465503ef47533d66d0c45c65e20861b803a7b17a728eab96_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'range'));*/
/* */
