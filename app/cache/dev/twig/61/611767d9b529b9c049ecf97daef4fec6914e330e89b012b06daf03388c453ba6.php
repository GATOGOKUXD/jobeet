<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_8f66598553c4062a795c4e421d731ac7a49b16ad13798454af7fe6d8ee31c1db extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9724111fc2d5f243aa74221ae415c45400e79ec994684c596dd8839962b2ade7 = $this->env->getExtension("native_profiler");
        $__internal_9724111fc2d5f243aa74221ae415c45400e79ec994684c596dd8839962b2ade7->enter($__internal_9724111fc2d5f243aa74221ae415c45400e79ec994684c596dd8839962b2ade7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_9724111fc2d5f243aa74221ae415c45400e79ec994684c596dd8839962b2ade7->leave($__internal_9724111fc2d5f243aa74221ae415c45400e79ec994684c596dd8839962b2ade7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'url')) ?>*/
/* */
