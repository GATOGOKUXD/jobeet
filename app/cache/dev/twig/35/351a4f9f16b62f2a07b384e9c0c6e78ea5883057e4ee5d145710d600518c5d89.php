<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_a5730bff8c35d7c6ce3b819f7fc361df67e5f371342ec38cafebbceb7aec1d3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f9c1ee7f7d612a96c543cf1e6121106ed47730744fe9e9bc32984fb036a246a2 = $this->env->getExtension("native_profiler");
        $__internal_f9c1ee7f7d612a96c543cf1e6121106ed47730744fe9e9bc32984fb036a246a2->enter($__internal_f9c1ee7f7d612a96c543cf1e6121106ed47730744fe9e9bc32984fb036a246a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_f9c1ee7f7d612a96c543cf1e6121106ed47730744fe9e9bc32984fb036a246a2->leave($__internal_f9c1ee7f7d612a96c543cf1e6121106ed47730744fe9e9bc32984fb036a246a2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
