<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_6d91c90f973553ca29692e0a61d04c34e42063c2a714a90330d40e52e0c0df96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_19c7c5741d8acbb7507fa5843e5d8df0b2206595a75d99f11cddf9f39355454e = $this->env->getExtension("native_profiler");
        $__internal_19c7c5741d8acbb7507fa5843e5d8df0b2206595a75d99f11cddf9f39355454e->enter($__internal_19c7c5741d8acbb7507fa5843e5d8df0b2206595a75d99f11cddf9f39355454e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_19c7c5741d8acbb7507fa5843e5d8df0b2206595a75d99f11cddf9f39355454e->leave($__internal_19c7c5741d8acbb7507fa5843e5d8df0b2206595a75d99f11cddf9f39355454e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_rows') ?>*/
/* */
