<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_b0695758e28ce023c6d11316214f5d9e5294995991809c1fddee4b01bfb88d51 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ede55aca1dda418e5be29f243f07172fca51e5089299e586882c1a5448f71660 = $this->env->getExtension("native_profiler");
        $__internal_ede55aca1dda418e5be29f243f07172fca51e5089299e586882c1a5448f71660->enter($__internal_ede55aca1dda418e5be29f243f07172fca51e5089299e586882c1a5448f71660_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.js.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_ede55aca1dda418e5be29f243f07172fca51e5089299e586882c1a5448f71660->leave($__internal_ede55aca1dda418e5be29f243f07172fca51e5089299e586882c1a5448f71660_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
