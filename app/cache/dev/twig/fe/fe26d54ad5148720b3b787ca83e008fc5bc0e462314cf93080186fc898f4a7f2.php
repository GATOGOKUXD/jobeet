<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_4d3619bd773875c7c6ae36435fb5723953ec49709549436850daed2e3f79ce92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66363075740f468352d6cd662ac4e4b0c4d697a3f6a5fe2846a094dca7ace362 = $this->env->getExtension("native_profiler");
        $__internal_66363075740f468352d6cd662ac4e4b0c4d697a3f6a5fe2846a094dca7ace362->enter($__internal_66363075740f468352d6cd662ac4e4b0c4d697a3f6a5fe2846a094dca7ace362_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_66363075740f468352d6cd662ac4e4b0c4d697a3f6a5fe2846a094dca7ace362->leave($__internal_66363075740f468352d6cd662ac4e4b0c4d697a3f6a5fe2846a094dca7ace362_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
