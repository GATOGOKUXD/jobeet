<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_10c134dbe3ad01146adacf6af975bffce3d9c207a50f223588404dddaf12b0dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c2018d4023de1d4f011c1daaf788c9446b0fc1c7c3aa4219c00ea939a8a8a0d0 = $this->env->getExtension("native_profiler");
        $__internal_c2018d4023de1d4f011c1daaf788c9446b0fc1c7c3aa4219c00ea939a8a8a0d0->enter($__internal_c2018d4023de1d4f011c1daaf788c9446b0fc1c7c3aa4219c00ea939a8a8a0d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_c2018d4023de1d4f011c1daaf788c9446b0fc1c7c3aa4219c00ea939a8a8a0d0->leave($__internal_c2018d4023de1d4f011c1daaf788c9446b0fc1c7c3aa4219c00ea939a8a8a0d0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </div>*/
/* */
