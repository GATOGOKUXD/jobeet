<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_6e17807fe8dec2130c5905ee3aeaa347028f979341a65e27b5e6351ce8d35081 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_307af3a8d27732a7a91917757f0c1fea1917b56e4f19f341a1bea0de6c48c6a9 = $this->env->getExtension("native_profiler");
        $__internal_307af3a8d27732a7a91917757f0c1fea1917b56e4f19f341a1bea0de6c48c6a9->enter($__internal_307af3a8d27732a7a91917757f0c1fea1917b56e4f19f341a1bea0de6c48c6a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_307af3a8d27732a7a91917757f0c1fea1917b56e4f19f341a1bea0de6c48c6a9->leave($__internal_307af3a8d27732a7a91917757f0c1fea1917b56e4f19f341a1bea0de6c48c6a9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
