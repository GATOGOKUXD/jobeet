<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_4759799ceb77349f3fdd5fff78bf830966fed076fe00971a98bffa3bd15e6f55 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_af4ecff9b5cc89d633e19ce01f9122afdf29a1fb17547390be4e7c484881f256 = $this->env->getExtension("native_profiler");
        $__internal_af4ecff9b5cc89d633e19ce01f9122afdf29a1fb17547390be4e7c484881f256->enter($__internal_af4ecff9b5cc89d633e19ce01f9122afdf29a1fb17547390be4e7c484881f256_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_af4ecff9b5cc89d633e19ce01f9122afdf29a1fb17547390be4e7c484881f256->leave($__internal_af4ecff9b5cc89d633e19ce01f9122afdf29a1fb17547390be4e7c484881f256_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div>*/
/*     <?php echo $view['form']->label($form) ?>*/
/*     <?php echo $view['form']->errors($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* </div>*/
/* */
