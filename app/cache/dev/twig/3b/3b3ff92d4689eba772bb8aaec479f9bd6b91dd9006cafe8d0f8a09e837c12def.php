<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_be6ef8431ec111e3ff43f779a75ea2a1f2b75607be09cd6eefdb9d69e98c872d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a14fbdb4669a7ba3109e5514df1f88b1b66ab36f13bd4deea25e06e3eb0e3090 = $this->env->getExtension("native_profiler");
        $__internal_a14fbdb4669a7ba3109e5514df1f88b1b66ab36f13bd4deea25e06e3eb0e3090->enter($__internal_a14fbdb4669a7ba3109e5514df1f88b1b66ab36f13bd4deea25e06e3eb0e3090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_a14fbdb4669a7ba3109e5514df1f88b1b66ab36f13bd4deea25e06e3eb0e3090->leave($__internal_a14fbdb4669a7ba3109e5514df1f88b1b66ab36f13bd4deea25e06e3eb0e3090_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (count($errors) > 0): ?>*/
/*     <ul>*/
/*         <?php foreach ($errors as $error): ?>*/
/*             <li><?php echo $error->getMessage() ?></li>*/
/*         <?php endforeach; ?>*/
/*     </ul>*/
/* <?php endif ?>*/
/* */
