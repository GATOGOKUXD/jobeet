<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_039eacc0e74e297c3a84792505ea44c5e1c6a3685224fa22810302aaa6bca6c4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fbadd52b96a67e02c3b87dc4c2e3d1508f776a3b8819c89f81e4a6fe58196987 = $this->env->getExtension("native_profiler");
        $__internal_fbadd52b96a67e02c3b87dc4c2e3d1508f776a3b8819c89f81e4a6fe58196987->enter($__internal_fbadd52b96a67e02c3b87dc4c2e3d1508f776a3b8819c89f81e4a6fe58196987_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fbadd52b96a67e02c3b87dc4c2e3d1508f776a3b8819c89f81e4a6fe58196987->leave($__internal_fbadd52b96a67e02c3b87dc4c2e3d1508f776a3b8819c89f81e4a6fe58196987_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_95f480247aef8a931d696e1fe51c3acdfed2b119caeb5cd15f454614f63aeb1a = $this->env->getExtension("native_profiler");
        $__internal_95f480247aef8a931d696e1fe51c3acdfed2b119caeb5cd15f454614f63aeb1a->enter($__internal_95f480247aef8a931d696e1fe51c3acdfed2b119caeb5cd15f454614f63aeb1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_95f480247aef8a931d696e1fe51c3acdfed2b119caeb5cd15f454614f63aeb1a->leave($__internal_95f480247aef8a931d696e1fe51c3acdfed2b119caeb5cd15f454614f63aeb1a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_713dca0751e2af1965ff27bf21249d905c9aa03a07d627deedf6d17e723c9fb6 = $this->env->getExtension("native_profiler");
        $__internal_713dca0751e2af1965ff27bf21249d905c9aa03a07d627deedf6d17e723c9fb6->enter($__internal_713dca0751e2af1965ff27bf21249d905c9aa03a07d627deedf6d17e723c9fb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_713dca0751e2af1965ff27bf21249d905c9aa03a07d627deedf6d17e723c9fb6->leave($__internal_713dca0751e2af1965ff27bf21249d905c9aa03a07d627deedf6d17e723c9fb6_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_a3bca21f038e4e6ba3dd1d1dbf97578a9976e5e3652a62cc81f9563eb64a0878 = $this->env->getExtension("native_profiler");
        $__internal_a3bca21f038e4e6ba3dd1d1dbf97578a9976e5e3652a62cc81f9563eb64a0878->enter($__internal_a3bca21f038e4e6ba3dd1d1dbf97578a9976e5e3652a62cc81f9563eb64a0878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_a3bca21f038e4e6ba3dd1d1dbf97578a9976e5e3652a62cc81f9563eb64a0878->leave($__internal_a3bca21f038e4e6ba3dd1d1dbf97578a9976e5e3652a62cc81f9563eb64a0878_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
