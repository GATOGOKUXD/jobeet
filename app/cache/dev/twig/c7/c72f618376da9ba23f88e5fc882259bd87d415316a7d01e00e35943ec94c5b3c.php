<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_e0c0cafd06cf3d5d945f92ee031a6acb80182e1803adfb0ee6fa900ef4f9a43c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_954ae05678eb70a4ddb220cb54fce78e9feccb59ec2d780a814c646cc1606ed9 = $this->env->getExtension("native_profiler");
        $__internal_954ae05678eb70a4ddb220cb54fce78e9feccb59ec2d780a814c646cc1606ed9->enter($__internal_954ae05678eb70a4ddb220cb54fce78e9feccb59ec2d780a814c646cc1606ed9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_954ae05678eb70a4ddb220cb54fce78e9feccb59ec2d780a814c646cc1606ed9->leave($__internal_954ae05678eb70a4ddb220cb54fce78e9feccb59ec2d780a814c646cc1606ed9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <table <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*     <?php if (!$form->parent && $errors): ?>*/
/*     <tr>*/
/*         <td colspan="2">*/
/*             <?php echo $view['form']->errors($form) ?>*/
/*         </td>*/
/*     </tr>*/
/*     <?php endif ?>*/
/*     <?php echo $view['form']->block($form, 'form_rows') ?>*/
/*     <?php echo $view['form']->rest($form) ?>*/
/* </table>*/
/* */
