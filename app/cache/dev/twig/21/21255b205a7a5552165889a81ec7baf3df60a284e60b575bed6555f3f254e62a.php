<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_17fb51d4dc48c66ad1fd17e7a270eaf273d746ceddc0e8b79e87d243a52b0743 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d47cae2d585b7340a85b8c388be15329df56057bf5ae55610058cfa94a1fc54b = $this->env->getExtension("native_profiler");
        $__internal_d47cae2d585b7340a85b8c388be15329df56057bf5ae55610058cfa94a1fc54b->enter($__internal_d47cae2d585b7340a85b8c388be15329df56057bf5ae55610058cfa94a1fc54b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_d47cae2d585b7340a85b8c388be15329df56057bf5ae55610058cfa94a1fc54b->leave($__internal_d47cae2d585b7340a85b8c388be15329df56057bf5ae55610058cfa94a1fc54b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'number')) ?>*/
/* */
