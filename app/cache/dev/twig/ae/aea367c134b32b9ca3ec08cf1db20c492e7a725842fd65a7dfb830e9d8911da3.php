<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_93e64abb384245904f7a1eaeba1c61870849036b20eaa2bf5d2bd9bcf94e2a8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95d519a43d9d2b469e3a97a99124da92767df832f7f68ee6777b9675e367a7e1 = $this->env->getExtension("native_profiler");
        $__internal_95d519a43d9d2b469e3a97a99124da92767df832f7f68ee6777b9675e367a7e1->enter($__internal_95d519a43d9d2b469e3a97a99124da92767df832f7f68ee6777b9675e367a7e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_95d519a43d9d2b469e3a97a99124da92767df832f7f68ee6777b9675e367a7e1->leave($__internal_95d519a43d9d2b469e3a97a99124da92767df832f7f68ee6777b9675e367a7e1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->widget($form) ?>*/
/* */
