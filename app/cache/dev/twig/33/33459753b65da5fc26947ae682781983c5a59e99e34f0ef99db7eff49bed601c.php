<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_6b4da0e3be505c9654e3dd9dceadd9f8c8e71758aed43171c04faa36933f1602 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca7b978ddefac5eaab554663ca2df216db29610715d234d958b9b37bcc3662fd = $this->env->getExtension("native_profiler");
        $__internal_ca7b978ddefac5eaab554663ca2df216db29610715d234d958b9b37bcc3662fd->enter($__internal_ca7b978ddefac5eaab554663ca2df216db29610715d234d958b9b37bcc3662fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca7b978ddefac5eaab554663ca2df216db29610715d234d958b9b37bcc3662fd->leave($__internal_ca7b978ddefac5eaab554663ca2df216db29610715d234d958b9b37bcc3662fd_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_4353031555845656131c0f383b7c674a8ffee7aa4c42ff92b90ec9d3b4a56f75 = $this->env->getExtension("native_profiler");
        $__internal_4353031555845656131c0f383b7c674a8ffee7aa4c42ff92b90ec9d3b4a56f75->enter($__internal_4353031555845656131c0f383b7c674a8ffee7aa4c42ff92b90ec9d3b4a56f75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_4353031555845656131c0f383b7c674a8ffee7aa4c42ff92b90ec9d3b4a56f75->leave($__internal_4353031555845656131c0f383b7c674a8ffee7aa4c42ff92b90ec9d3b4a56f75_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_b7bbe6ceb5ec95026375912325ace17fec2c2bffdbc68b29125caf8473f251e7 = $this->env->getExtension("native_profiler");
        $__internal_b7bbe6ceb5ec95026375912325ace17fec2c2bffdbc68b29125caf8473f251e7->enter($__internal_b7bbe6ceb5ec95026375912325ace17fec2c2bffdbc68b29125caf8473f251e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_b7bbe6ceb5ec95026375912325ace17fec2c2bffdbc68b29125caf8473f251e7->leave($__internal_b7bbe6ceb5ec95026375912325ace17fec2c2bffdbc68b29125caf8473f251e7_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
