<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_ac16051d4ce30d444a9f1a93e8fffab0b9dd85bb1552f94504676c0e445c9825 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_14c16f8df2b604b95e59e327343cd6c594db3f05bc73c2bed2cea892c09db136 = $this->env->getExtension("native_profiler");
        $__internal_14c16f8df2b604b95e59e327343cd6c594db3f05bc73c2bed2cea892c09db136->enter($__internal_14c16f8df2b604b95e59e327343cd6c594db3f05bc73c2bed2cea892c09db136_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_14c16f8df2b604b95e59e327343cd6c594db3f05bc73c2bed2cea892c09db136->leave($__internal_14c16f8df2b604b95e59e327343cd6c594db3f05bc73c2bed2cea892c09db136_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'email')) ?>*/
/* */
