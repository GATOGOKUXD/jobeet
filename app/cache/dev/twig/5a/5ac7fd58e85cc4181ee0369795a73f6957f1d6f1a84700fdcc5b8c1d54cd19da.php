<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_1e658e77721380e7a648e1cd747251101731d6d4ca4423174b85960bc31e9559 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26efc91c2a00696d0a339a9083a1e2c8f3ec7ac8887d0962f5212efd116d86ee = $this->env->getExtension("native_profiler");
        $__internal_26efc91c2a00696d0a339a9083a1e2c8f3ec7ac8887d0962f5212efd116d86ee->enter($__internal_26efc91c2a00696d0a339a9083a1e2c8f3ec7ac8887d0962f5212efd116d86ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_26efc91c2a00696d0a339a9083a1e2c8f3ec7ac8887d0962f5212efd116d86ee->leave($__internal_26efc91c2a00696d0a339a9083a1e2c8f3ec7ac8887d0962f5212efd116d86ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($form->vars['multipart']): ?>enctype="multipart/form-data"<?php endif ?>*/
/* */
