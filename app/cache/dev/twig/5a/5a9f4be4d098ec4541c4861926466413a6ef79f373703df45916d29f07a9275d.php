<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_891ab3cb532e8eab46ea243926dfa82b2cb55edfb9e715bcc0d109362eb142cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6fe2839cd77b46669139a90822b038afcb23bd76ad146aba410c51e922447ebf = $this->env->getExtension("native_profiler");
        $__internal_6fe2839cd77b46669139a90822b038afcb23bd76ad146aba410c51e922447ebf->enter($__internal_6fe2839cd77b46669139a90822b038afcb23bd76ad146aba410c51e922447ebf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_6fe2839cd77b46669139a90822b038afcb23bd76ad146aba410c51e922447ebf->leave($__internal_6fe2839cd77b46669139a90822b038afcb23bd76ad146aba410c51e922447ebf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo str_replace('{{ widget }}', $view['form']->block($form, 'form_widget_simple'), $money_pattern) ?>*/
/* */
