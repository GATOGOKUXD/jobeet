<?php

/* :job:new.html.twig */
class __TwigTemplate_8828696e8e8602972efbbd24ea9fd8b136569378c1e28741ef003400f1ffe35b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", ":job:new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79adda5d75aee7e29fe598ca7d8f857afbb6a9476c6f0ea1df72f714c30884b8 = $this->env->getExtension("native_profiler");
        $__internal_79adda5d75aee7e29fe598ca7d8f857afbb6a9476c6f0ea1df72f714c30884b8->enter($__internal_79adda5d75aee7e29fe598ca7d8f857afbb6a9476c6f0ea1df72f714c30884b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":job:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_79adda5d75aee7e29fe598ca7d8f857afbb6a9476c6f0ea1df72f714c30884b8->leave($__internal_79adda5d75aee7e29fe598ca7d8f857afbb6a9476c6f0ea1df72f714c30884b8_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_84f2f3e5ff7f08441bbf7590e403c7ea968a7468a845cee7ae02e0e0d72d8417 = $this->env->getExtension("native_profiler");
        $__internal_84f2f3e5ff7f08441bbf7590e403c7ea968a7468a845cee7ae02e0e0d72d8417->enter($__internal_84f2f3e5ff7f08441bbf7590e403c7ea968a7468a845cee7ae02e0e0d72d8417_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<h1>Job creation</h1>
 <div id=\"job_form\">
 ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
 
 </div>
 <ul class=\"record_actions\">
 <li>
 <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("job_new");
        echo "\">
 Back to the list
 </a>
 </li>
 </ul>
";
        
        $__internal_84f2f3e5ff7f08441bbf7590e403c7ea968a7468a845cee7ae02e0e0d72d8417->leave($__internal_84f2f3e5ff7f08441bbf7590e403c7ea968a7468a845cee7ae02e0e0d72d8417_prof);

    }

    public function getTemplateName()
    {
        return ":job:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content -%}*/
/*  <h1>Job creation</h1>*/
/*  <div id="job_form">*/
/*  {{ form(form) }}*/
/*  */
/*  </div>*/
/*  <ul class="record_actions">*/
/*  <li>*/
/*  <a href="{{ path('job_new') }}">*/
/*  Back to the list*/
/*  </a>*/
/*  </li>*/
/*  </ul>*/
/* {% endblock %} */
/* */
/* */
