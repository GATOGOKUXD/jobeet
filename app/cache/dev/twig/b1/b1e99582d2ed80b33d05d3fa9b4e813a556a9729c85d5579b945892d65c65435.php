<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_6d2141ac8d92d2ac5ad7e854845ad2d76f3f025ce48c30348330fca0058e6540 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66d12fd4ff2e8125069c5d3a18497adea2e152010c9c97f916d49511e793cd71 = $this->env->getExtension("native_profiler");
        $__internal_66d12fd4ff2e8125069c5d3a18497adea2e152010c9c97f916d49511e793cd71->enter($__internal_66d12fd4ff2e8125069c5d3a18497adea2e152010c9c97f916d49511e793cd71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_66d12fd4ff2e8125069c5d3a18497adea2e152010c9c97f916d49511e793cd71->leave($__internal_66d12fd4ff2e8125069c5d3a18497adea2e152010c9c97f916d49511e793cd71_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!isset($render_rest) || $render_rest): ?>*/
/* <?php echo $view['form']->rest($form) ?>*/
/* <?php endif ?>*/
/* </form>*/
/* */
