<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_8044e067f21ea1b0654ba324682e5bd6925a8a0ba2f7ade09f04c35440a0adc3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_142de88b3388f848847fe9b3b4324bb0edb62f46f9a8d348fd96981f3756bcac = $this->env->getExtension("native_profiler");
        $__internal_142de88b3388f848847fe9b3b4324bb0edb62f46f9a8d348fd96981f3756bcac->enter($__internal_142de88b3388f848847fe9b3b4324bb0edb62f46f9a8d348fd96981f3756bcac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_142de88b3388f848847fe9b3b4324bb0edb62f46f9a8d348fd96981f3756bcac->leave($__internal_142de88b3388f848847fe9b3b4324bb0edb62f46f9a8d348fd96981f3756bcac_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?>*/
/* */
