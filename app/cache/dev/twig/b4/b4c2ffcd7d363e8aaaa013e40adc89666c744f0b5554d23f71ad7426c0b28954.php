<?php

/* job/new.html.twig */
class __TwigTemplate_e6069dbfb01f92ef4a9926e8677d0e1144d12499f714eeaa4876fc6e5732402c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "job/new.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ce38ae5108640c8c440ab74156052daff036844094aa053dbaaf0c5d4546db7 = $this->env->getExtension("native_profiler");
        $__internal_6ce38ae5108640c8c440ab74156052daff036844094aa053dbaaf0c5d4546db7->enter($__internal_6ce38ae5108640c8c440ab74156052daff036844094aa053dbaaf0c5d4546db7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "job/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6ce38ae5108640c8c440ab74156052daff036844094aa053dbaaf0c5d4546db7->leave($__internal_6ce38ae5108640c8c440ab74156052daff036844094aa053dbaaf0c5d4546db7_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_2b334339ef3cc1f4918ce35314e1d4749de12290c781bd260568ac40eb07c23d = $this->env->getExtension("native_profiler");
        $__internal_2b334339ef3cc1f4918ce35314e1d4749de12290c781bd260568ac40eb07c23d->enter($__internal_2b334339ef3cc1f4918ce35314e1d4749de12290c781bd260568ac40eb07c23d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "<h1>Job creation</h1>
 <div id=\"job_form\">
 ";
        // line 5
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
 
 </div>
 <ul class=\"record_actions\">
 <li>
 <a href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("job_new");
        echo "\">
 Back to the list
 </a>
 </li>
 </ul>
";
        
        $__internal_2b334339ef3cc1f4918ce35314e1d4749de12290c781bd260568ac40eb07c23d->leave($__internal_2b334339ef3cc1f4918ce35314e1d4749de12290c781bd260568ac40eb07c23d_prof);

    }

    public function getTemplateName()
    {
        return "job/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  44 => 5,  40 => 3,  34 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block content -%}*/
/*  <h1>Job creation</h1>*/
/*  <div id="job_form">*/
/*  {{ form(form) }}*/
/*  */
/*  </div>*/
/*  <ul class="record_actions">*/
/*  <li>*/
/*  <a href="{{ path('job_new') }}">*/
/*  Back to the list*/
/*  </a>*/
/*  </li>*/
/*  </ul>*/
/* {% endblock %} */
/* */
/* */
