<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_e0de77f57bdcf18e690d45bf08cfec3ee5490a43b18b9647500532f87979ec53 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53dcf1e82e87eab3c38c01f02493264855ad69e050963d412e0355efefa9d7b7 = $this->env->getExtension("native_profiler");
        $__internal_53dcf1e82e87eab3c38c01f02493264855ad69e050963d412e0355efefa9d7b7->enter($__internal_53dcf1e82e87eab3c38c01f02493264855ad69e050963d412e0355efefa9d7b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_53dcf1e82e87eab3c38c01f02493264855ad69e050963d412e0355efefa9d7b7->leave($__internal_53dcf1e82e87eab3c38c01f02493264855ad69e050963d412e0355efefa9d7b7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple', array('type' => isset($type) ? $type : 'hidden')) ?>*/
/* */
