<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_dd7043a8d327da8c7e4b34337563ed27fc12bded07055a635cddc77817e1092a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2abbed58070adcd68f28e2c452ac636300e38f4509c40e72394f92f9dc1edd09 = $this->env->getExtension("native_profiler");
        $__internal_2abbed58070adcd68f28e2c452ac636300e38f4509c40e72394f92f9dc1edd09->enter($__internal_2abbed58070adcd68f28e2c452ac636300e38f4509c40e72394f92f9dc1edd09_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_2abbed58070adcd68f28e2c452ac636300e38f4509c40e72394f92f9dc1edd09->leave($__internal_2abbed58070adcd68f28e2c452ac636300e38f4509c40e72394f92f9dc1edd09_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child): ?>*/
/*     <?php if (!$child->isRendered()): ?>*/
/*         <?php echo $view['form']->row($child) ?>*/
/*     <?php endif; ?>*/
/* <?php endforeach; ?>*/
/* */
