<?php

/* @Twig/Exception/exception.css.twig */
class __TwigTemplate_57ed7760e3a047fe23cddb80e415e173c055390de0d923f07ebc8e116ff992d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e348a0e185be1886842dd6ce11cb2bdc2a65a031f17cf311df54c8260af7d99e = $this->env->getExtension("native_profiler");
        $__internal_e348a0e185be1886842dd6ce11cb2bdc2a65a031f17cf311df54c8260af7d99e->enter($__internal_e348a0e185be1886842dd6ce11cb2bdc2a65a031f17cf311df54c8260af7d99e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "@Twig/Exception/exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_e348a0e185be1886842dd6ce11cb2bdc2a65a031f17cf311df54c8260af7d99e->leave($__internal_e348a0e185be1886842dd6ce11cb2bdc2a65a031f17cf311df54c8260af7d99e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
