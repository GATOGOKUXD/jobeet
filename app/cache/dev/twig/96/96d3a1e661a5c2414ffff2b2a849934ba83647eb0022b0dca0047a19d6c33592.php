<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_e4586ab4c8a693074b175c09b417b47f7dee69e147961788f2dd651dc9c7adda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec9aa99573f575915159bd4565c144429c603f1ec6def2b00f7f6cbed2e3d101 = $this->env->getExtension("native_profiler");
        $__internal_ec9aa99573f575915159bd4565c144429c603f1ec6def2b00f7f6cbed2e3d101->enter($__internal_ec9aa99573f575915159bd4565c144429c603f1ec6def2b00f7f6cbed2e3d101_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_ec9aa99573f575915159bd4565c144429c603f1ec6def2b00f7f6cbed2e3d101->leave($__internal_ec9aa99573f575915159bd4565c144429c603f1ec6def2b00f7f6cbed2e3d101_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'search')) ?>*/
/* */
