<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_4550c1f911c5cbf971017a4199e70bdcfece2fcfa74470c330f84de259ebd232 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fdb1d3f1f0234505bdf65543e9f8bdf79a737c6c0d118be6d569983df0929940 = $this->env->getExtension("native_profiler");
        $__internal_fdb1d3f1f0234505bdf65543e9f8bdf79a737c6c0d118be6d569983df0929940->enter($__internal_fdb1d3f1f0234505bdf65543e9f8bdf79a737c6c0d118be6d569983df0929940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_fdb1d3f1f0234505bdf65543e9f8bdf79a737c6c0d118be6d569983df0929940->leave($__internal_fdb1d3f1f0234505bdf65543e9f8bdf79a737c6c0d118be6d569983df0929940_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
