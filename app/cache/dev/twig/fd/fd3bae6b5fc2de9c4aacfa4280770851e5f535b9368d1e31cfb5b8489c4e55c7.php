<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_a7f2d0966b4dca201d5a06c0ae6ab9fbc9b78bb6b4cfa3fc3a9778b77e0b9f14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a315b6c924918a78f417d711ecbf0f15c34ab1d8491bcaab455e9790b123f43f = $this->env->getExtension("native_profiler");
        $__internal_a315b6c924918a78f417d711ecbf0f15c34ab1d8491bcaab455e9790b123f43f->enter($__internal_a315b6c924918a78f417d711ecbf0f15c34ab1d8491bcaab455e9790b123f43f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_a315b6c924918a78f417d711ecbf0f15c34ab1d8491bcaab455e9790b123f43f->leave($__internal_a315b6c924918a78f417d711ecbf0f15c34ab1d8491bcaab455e9790b123f43f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
