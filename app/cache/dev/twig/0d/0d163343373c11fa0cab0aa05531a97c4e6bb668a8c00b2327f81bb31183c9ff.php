<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_50758f50753abdc689112483f9b91ff7246bdd5b8ee634c7eb162ebb35a8c2e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dc12e9036d1e9b5fd826629635338bf4ab8c6657a4fffc631bdb24024feef775 = $this->env->getExtension("native_profiler");
        $__internal_dc12e9036d1e9b5fd826629635338bf4ab8c6657a4fffc631bdb24024feef775->enter($__internal_dc12e9036d1e9b5fd826629635338bf4ab8c6657a4fffc631bdb24024feef775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_dc12e9036d1e9b5fd826629635338bf4ab8c6657a4fffc631bdb24024feef775->leave($__internal_dc12e9036d1e9b5fd826629635338bf4ab8c6657a4fffc631bdb24024feef775_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'text')) ?> %*/
/* */
