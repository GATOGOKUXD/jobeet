<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_ac956497e4fdda5f47e2b4b2a1215fd8263914414f4666d863be8308c0589e29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fe2428be3e1fee2cd54252506f35ed61f5049290af624d0636e4d95a0773864 = $this->env->getExtension("native_profiler");
        $__internal_5fe2428be3e1fee2cd54252506f35ed61f5049290af624d0636e4d95a0773864->enter($__internal_5fe2428be3e1fee2cd54252506f35ed61f5049290af624d0636e4d95a0773864_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5fe2428be3e1fee2cd54252506f35ed61f5049290af624d0636e4d95a0773864->leave($__internal_5fe2428be3e1fee2cd54252506f35ed61f5049290af624d0636e4d95a0773864_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_11df0d5c52bbc21685926c68bc0303e2221e241a5c51e41b35e47b90e5b25633 = $this->env->getExtension("native_profiler");
        $__internal_11df0d5c52bbc21685926c68bc0303e2221e241a5c51e41b35e47b90e5b25633->enter($__internal_11df0d5c52bbc21685926c68bc0303e2221e241a5c51e41b35e47b90e5b25633_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_11df0d5c52bbc21685926c68bc0303e2221e241a5c51e41b35e47b90e5b25633->leave($__internal_11df0d5c52bbc21685926c68bc0303e2221e241a5c51e41b35e47b90e5b25633_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_637f6d3a5b1c2633284aec9577bf1e785a5033a642f72196a92521fd54b5c857 = $this->env->getExtension("native_profiler");
        $__internal_637f6d3a5b1c2633284aec9577bf1e785a5033a642f72196a92521fd54b5c857->enter($__internal_637f6d3a5b1c2633284aec9577bf1e785a5033a642f72196a92521fd54b5c857_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_637f6d3a5b1c2633284aec9577bf1e785a5033a642f72196a92521fd54b5c857->leave($__internal_637f6d3a5b1c2633284aec9577bf1e785a5033a642f72196a92521fd54b5c857_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_4cb6680e4d82bc96fde5584c5f70ad4d5a5323ef1283b6d3e3e5b42d660477db = $this->env->getExtension("native_profiler");
        $__internal_4cb6680e4d82bc96fde5584c5f70ad4d5a5323ef1283b6d3e3e5b42d660477db->enter($__internal_4cb6680e4d82bc96fde5584c5f70ad4d5a5323ef1283b6d3e3e5b42d660477db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_4cb6680e4d82bc96fde5584c5f70ad4d5a5323ef1283b6d3e3e5b42d660477db->leave($__internal_4cb6680e4d82bc96fde5584c5f70ad4d5a5323ef1283b6d3e3e5b42d660477db_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
