<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_e649c6e54f1ad12dd078ad71d9833e65f7d0b5601a306a5f36316e96d77f8582 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c9e11abf528abf5179adb64e81445fd695f3b7b77314f8f3c098772fbc226a7 = $this->env->getExtension("native_profiler");
        $__internal_2c9e11abf528abf5179adb64e81445fd695f3b7b77314f8f3c098772fbc226a7->enter($__internal_2c9e11abf528abf5179adb64e81445fd695f3b7b77314f8f3c098772fbc226a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_2c9e11abf528abf5179adb64e81445fd695f3b7b77314f8f3c098772fbc226a7->leave($__internal_2c9e11abf528abf5179adb64e81445fd695f3b7b77314f8f3c098772fbc226a7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}*/
/* *//* */
/* */
