<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_6f61a39b7bf2d5e6c7fb13562bdcb99e01f54ef820e675566acba4900301120f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e63dbf96f66e4e02a5252290d5fbe7a247a4c4b587afce2aef841158e296a7b = $this->env->getExtension("native_profiler");
        $__internal_0e63dbf96f66e4e02a5252290d5fbe7a247a4c4b587afce2aef841158e296a7b->enter($__internal_0e63dbf96f66e4e02a5252290d5fbe7a247a4c4b587afce2aef841158e296a7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_0e63dbf96f66e4e02a5252290d5fbe7a247a4c4b587afce2aef841158e296a7b->leave($__internal_0e63dbf96f66e4e02a5252290d5fbe7a247a4c4b587afce2aef841158e296a7b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
