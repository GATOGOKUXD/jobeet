<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_0650a8d006893d7bdc71e9bead9904503a439e8caf039042b3b66554c6dc9bb2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d51e1d80cb181b19fd01ae2c69470e070d1074fd75a20d6d05d6ec0a17b692e = $this->env->getExtension("native_profiler");
        $__internal_5d51e1d80cb181b19fd01ae2c69470e070d1074fd75a20d6d05d6ec0a17b692e->enter($__internal_5d51e1d80cb181b19fd01ae2c69470e070d1074fd75a20d6d05d6ec0a17b692e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_5d51e1d80cb181b19fd01ae2c69470e070d1074fd75a20d6d05d6ec0a17b692e->leave($__internal_5d51e1d80cb181b19fd01ae2c69470e070d1074fd75a20d6d05d6ec0a17b692e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }
}
/* /**/
/* {{ status_code }} {{ status_text }}*/
/* */
/* *//* */
/* */
