<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_4db2f8e0b26369ee01bd19b795f29e06e5898d8ce531116ed4bb06dabcbc3b4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ffe872a1e2ef7072adb22fdb5efaec3a448c4d86b6e66ebc78c389092df14640 = $this->env->getExtension("native_profiler");
        $__internal_ffe872a1e2ef7072adb22fdb5efaec3a448c4d86b6e66ebc78c389092df14640->enter($__internal_ffe872a1e2ef7072adb22fdb5efaec3a448c4d86b6e66ebc78c389092df14640_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_ffe872a1e2ef7072adb22fdb5efaec3a448c4d86b6e66ebc78c389092df14640->leave($__internal_ffe872a1e2ef7072adb22fdb5efaec3a448c4d86b6e66ebc78c389092df14640_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="checkbox"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     <?php if (strlen($value) > 0): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?>*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
