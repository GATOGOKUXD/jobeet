<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_12a78854de1534c157514c404730e737f12bd5535b0856b155fa826e754f0f1e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f13d9eaad11af33599f94c47253e46c9bd9c088ba22652fca24111f6f15f614 = $this->env->getExtension("native_profiler");
        $__internal_8f13d9eaad11af33599f94c47253e46c9bd9c088ba22652fca24111f6f15f614->enter($__internal_8f13d9eaad11af33599f94c47253e46c9bd9c088ba22652fca24111f6f15f614_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f13d9eaad11af33599f94c47253e46c9bd9c088ba22652fca24111f6f15f614->leave($__internal_8f13d9eaad11af33599f94c47253e46c9bd9c088ba22652fca24111f6f15f614_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_bc018212e53be92a167aec8d32f7d038caca00eb9c5bb3ce1c5f654faefcfcb6 = $this->env->getExtension("native_profiler");
        $__internal_bc018212e53be92a167aec8d32f7d038caca00eb9c5bb3ce1c5f654faefcfcb6->enter($__internal_bc018212e53be92a167aec8d32f7d038caca00eb9c5bb3ce1c5f654faefcfcb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_bc018212e53be92a167aec8d32f7d038caca00eb9c5bb3ce1c5f654faefcfcb6->leave($__internal_bc018212e53be92a167aec8d32f7d038caca00eb9c5bb3ce1c5f654faefcfcb6_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ccb854e55a3d3e1c88693d274a344e966b9a3a640cbd7c2e386a2bce09fedd9d = $this->env->getExtension("native_profiler");
        $__internal_ccb854e55a3d3e1c88693d274a344e966b9a3a640cbd7c2e386a2bce09fedd9d->enter($__internal_ccb854e55a3d3e1c88693d274a344e966b9a3a640cbd7c2e386a2bce09fedd9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_ccb854e55a3d3e1c88693d274a344e966b9a3a640cbd7c2e386a2bce09fedd9d->leave($__internal_ccb854e55a3d3e1c88693d274a344e966b9a3a640cbd7c2e386a2bce09fedd9d_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block title 'Redirection Intercepted' %}*/
/* */
/* {% block body %}*/
/*     <div class="sf-reset">*/
/*         <div class="block-exception">*/
/*             <h1>This request redirects to <a href="{{ location }}">{{ location }}</a>.</h1>*/
/* */
/*             <p>*/
/*                 <small>*/
/*                     The redirect was intercepted by the web debug toolbar to help debugging.*/
/*                     For more information, see the "intercept-redirects" option of the Profiler.*/
/*                 </small>*/
/*             </p>*/
/*         </div>*/
/*     </div>*/
/* {% endblock %}*/
/* */
