<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_1c71dc5074c393325a0920694ea518c5a2da35bbd3cfde9274f8220eeaa6286c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2fa4f594f6dca3d3ea2808ebe297a17243220f333a8471842bde116ed8328232 = $this->env->getExtension("native_profiler");
        $__internal_2fa4f594f6dca3d3ea2808ebe297a17243220f333a8471842bde116ed8328232->enter($__internal_2fa4f594f6dca3d3ea2808ebe297a17243220f333a8471842bde116ed8328232_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_2fa4f594f6dca3d3ea2808ebe297a17243220f333a8471842bde116ed8328232->leave($__internal_2fa4f594f6dca3d3ea2808ebe297a17243220f333a8471842bde116ed8328232_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_container_attributes') ?>*/
/* */
