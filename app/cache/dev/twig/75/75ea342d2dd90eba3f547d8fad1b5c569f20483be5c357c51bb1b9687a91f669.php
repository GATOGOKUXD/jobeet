<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_b8584b91c2e52dd314b1c06ccee0ebec88178acaec924ce9c75c65b8629eb73c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c8270484dcb1e2423ca4574946e087c04e161eca09ab7a11fccfd973aba18096 = $this->env->getExtension("native_profiler");
        $__internal_c8270484dcb1e2423ca4574946e087c04e161eca09ab7a11fccfd973aba18096->enter($__internal_c8270484dcb1e2423ca4574946e087c04e161eca09ab7a11fccfd973aba18096_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_c8270484dcb1e2423ca4574946e087c04e161eca09ab7a11fccfd973aba18096->leave($__internal_c8270484dcb1e2423ca4574946e087c04e161eca09ab7a11fccfd973aba18096_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_0df84d16ff33b6f7ad450cec4894ca776b77a07e7ba1d38ae2a22ae2c795ffc3 = $this->env->getExtension("native_profiler");
        $__internal_0df84d16ff33b6f7ad450cec4894ca776b77a07e7ba1d38ae2a22ae2c795ffc3->enter($__internal_0df84d16ff33b6f7ad450cec4894ca776b77a07e7ba1d38ae2a22ae2c795ffc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_0df84d16ff33b6f7ad450cec4894ca776b77a07e7ba1d38ae2a22ae2c795ffc3->leave($__internal_0df84d16ff33b6f7ad450cec4894ca776b77a07e7ba1d38ae2a22ae2c795ffc3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
