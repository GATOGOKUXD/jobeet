<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_d22e30823d43066d716579c960310260a7bc15f6f4dd71e22234fc514183039d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4900fe8a615ccf83c22501bbf46c0a2d440a2a582240dbdc01c0c64a64c92052 = $this->env->getExtension("native_profiler");
        $__internal_4900fe8a615ccf83c22501bbf46c0a2d440a2a582240dbdc01c0c64a64c92052->enter($__internal_4900fe8a615ccf83c22501bbf46c0a2d440a2a582240dbdc01c0c64a64c92052_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_4900fe8a615ccf83c22501bbf46c0a2d440a2a582240dbdc01c0c64a64c92052->leave($__internal_4900fe8a615ccf83c22501bbf46c0a2d440a2a582240dbdc01c0c64a64c92052_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/* */
