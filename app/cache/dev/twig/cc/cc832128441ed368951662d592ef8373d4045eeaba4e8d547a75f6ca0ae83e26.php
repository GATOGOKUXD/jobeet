<?php

/* @Twig/Exception/error.txt.twig */
class __TwigTemplate_5701f4d4ed42b8e896d54b07ac0d3c06b82ee486d0184c8065ef75797d198c36 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b68d4aa3f68f520d7c983257d1962c7e3233aad98791fc9696fcaf1b9e2cd5b = $this->env->getExtension("native_profiler");
        $__internal_2b68d4aa3f68f520d7c983257d1962c7e3233aad98791fc9696fcaf1b9e2cd5b->enter($__internal_2b68d4aa3f68f520d7c983257d1962c7e3233aad98791fc9696fcaf1b9e2cd5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_2b68d4aa3f68f520d7c983257d1962c7e3233aad98791fc9696fcaf1b9e2cd5b->leave($__internal_2b68d4aa3f68f520d7c983257d1962c7e3233aad98791fc9696fcaf1b9e2cd5b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
