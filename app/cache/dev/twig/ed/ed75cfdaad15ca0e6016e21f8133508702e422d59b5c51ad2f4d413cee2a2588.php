<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_7bbde4708644998b3dcd08763bf4f63f05853455e8a0a70e827ab5099c8b4016 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2dba0d7c2f80c1dc33c11dd8ca7796adc161163560c233470c4bdd87b648b407 = $this->env->getExtension("native_profiler");
        $__internal_2dba0d7c2f80c1dc33c11dd8ca7796adc161163560c233470c4bdd87b648b407->enter($__internal_2dba0d7c2f80c1dc33c11dd8ca7796adc161163560c233470c4bdd87b648b407_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_2dba0d7c2f80c1dc33c11dd8ca7796adc161163560c233470c4bdd87b648b407->leave($__internal_2dba0d7c2f80c1dc33c11dd8ca7796adc161163560c233470c4bdd87b648b407_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="radio"*/
/*     <?php echo $view['form']->block($form, 'widget_attributes') ?>*/
/*     value="<?php echo $view->escape($value) ?>"*/
/*     <?php if ($checked): ?> checked="checked"<?php endif ?>*/
/* />*/
/* */
