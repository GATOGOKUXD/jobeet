<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e97fc69216ad511d184296c1c89b024a0687b6fada39f78cd258db6beed1426a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5398387413f822af4d711256c01efeaccaf2c5f229eb6162c2e519f97fafb69d = $this->env->getExtension("native_profiler");
        $__internal_5398387413f822af4d711256c01efeaccaf2c5f229eb6162c2e519f97fafb69d->enter($__internal_5398387413f822af4d711256c01efeaccaf2c5f229eb6162c2e519f97fafb69d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5398387413f822af4d711256c01efeaccaf2c5f229eb6162c2e519f97fafb69d->leave($__internal_5398387413f822af4d711256c01efeaccaf2c5f229eb6162c2e519f97fafb69d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_577ff64c7032dd2081246c3c6de9d51d2b0a32e707838de629901ba804788916 = $this->env->getExtension("native_profiler");
        $__internal_577ff64c7032dd2081246c3c6de9d51d2b0a32e707838de629901ba804788916->enter($__internal_577ff64c7032dd2081246c3c6de9d51d2b0a32e707838de629901ba804788916_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_577ff64c7032dd2081246c3c6de9d51d2b0a32e707838de629901ba804788916->leave($__internal_577ff64c7032dd2081246c3c6de9d51d2b0a32e707838de629901ba804788916_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_3f8c8a5b709ae3ae81239693388204ea4fde8aaff8bea1e80ee0cc80801ecac8 = $this->env->getExtension("native_profiler");
        $__internal_3f8c8a5b709ae3ae81239693388204ea4fde8aaff8bea1e80ee0cc80801ecac8->enter($__internal_3f8c8a5b709ae3ae81239693388204ea4fde8aaff8bea1e80ee0cc80801ecac8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_3f8c8a5b709ae3ae81239693388204ea4fde8aaff8bea1e80ee0cc80801ecac8->leave($__internal_3f8c8a5b709ae3ae81239693388204ea4fde8aaff8bea1e80ee0cc80801ecac8_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_14920f1bca2c0d11e49a65f173f384a66001903ebeb405e561bace10b9a89660 = $this->env->getExtension("native_profiler");
        $__internal_14920f1bca2c0d11e49a65f173f384a66001903ebeb405e561bace10b9a89660->enter($__internal_14920f1bca2c0d11e49a65f173f384a66001903ebeb405e561bace10b9a89660_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_14920f1bca2c0d11e49a65f173f384a66001903ebeb405e561bace10b9a89660->leave($__internal_14920f1bca2c0d11e49a65f173f384a66001903ebeb405e561bace10b9a89660_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
