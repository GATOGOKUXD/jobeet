<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_3ac20dd6942ee26ccc651e405eb6409fe43b5194a3f0bc484c14b4504355e397 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a13ee301e3a074bb4a0e52de43a2e10b38ac884b8ee0caf109f48da418d67926 = $this->env->getExtension("native_profiler");
        $__internal_a13ee301e3a074bb4a0e52de43a2e10b38ac884b8ee0caf109f48da418d67926->enter($__internal_a13ee301e3a074bb4a0e52de43a2e10b38ac884b8ee0caf109f48da418d67926_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_a13ee301e3a074bb4a0e52de43a2e10b38ac884b8ee0caf109f48da418d67926->leave($__internal_a13ee301e3a074bb4a0e52de43a2e10b38ac884b8ee0caf109f48da418d67926_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}*/
/* */
