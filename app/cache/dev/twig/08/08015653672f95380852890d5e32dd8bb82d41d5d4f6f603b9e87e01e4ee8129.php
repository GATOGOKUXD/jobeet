<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_4b8be430eb8e5d4454137653de1c26df958638fe3c7b89caf1dc0a6aefc3bb41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7ba6333f2e5eb5db4731835a5045933c55d60b4646a9dcf3026fed7bd8c6314a = $this->env->getExtension("native_profiler");
        $__internal_7ba6333f2e5eb5db4731835a5045933c55d60b4646a9dcf3026fed7bd8c6314a->enter($__internal_7ba6333f2e5eb5db4731835a5045933c55d60b4646a9dcf3026fed7bd8c6314a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_7ba6333f2e5eb5db4731835a5045933c55d60b4646a9dcf3026fed7bd8c6314a->leave($__internal_7ba6333f2e5eb5db4731835a5045933c55d60b4646a9dcf3026fed7bd8c6314a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/* <?php foreach ($form as $child): ?>*/
/*     <?php echo $view['form']->widget($child) ?>*/
/*     <?php echo $view['form']->label($child, null, array('translation_domain' => $choice_translation_domain)) ?>*/
/* <?php endforeach ?>*/
/* </div>*/
/* */
