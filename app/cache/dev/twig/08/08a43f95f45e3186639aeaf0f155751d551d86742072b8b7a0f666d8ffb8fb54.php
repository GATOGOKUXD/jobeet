<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_27d563d25500760935d5bd0efffbb281b0a29c51cbc50559287892d8485a0b08 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_128da380a0600ecc2b6c98cefb1ec723f3c81f856c097b4ce9bf238e214831f5 = $this->env->getExtension("native_profiler");
        $__internal_128da380a0600ecc2b6c98cefb1ec723f3c81f856c097b4ce9bf238e214831f5->enter($__internal_128da380a0600ecc2b6c98cefb1ec723f3c81f856c097b4ce9bf238e214831f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_128da380a0600ecc2b6c98cefb1ec723f3c81f856c097b4ce9bf238e214831f5->leave($__internal_128da380a0600ecc2b6c98cefb1ec723f3c81f856c097b4ce9bf238e214831f5_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_cf7afcf992a39f2a43e99ea982e12e84f7afb897fb83a2d82974041d4095d8c4 = $this->env->getExtension("native_profiler");
        $__internal_cf7afcf992a39f2a43e99ea982e12e84f7afb897fb83a2d82974041d4095d8c4->enter($__internal_cf7afcf992a39f2a43e99ea982e12e84f7afb897fb83a2d82974041d4095d8c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_cf7afcf992a39f2a43e99ea982e12e84f7afb897fb83a2d82974041d4095d8c4->leave($__internal_cf7afcf992a39f2a43e99ea982e12e84f7afb897fb83a2d82974041d4095d8c4_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_4e6496fa4cc2ffbd812649ef89b4643ead0cd28da611df30a391ab550f2cdb0a = $this->env->getExtension("native_profiler");
        $__internal_4e6496fa4cc2ffbd812649ef89b4643ead0cd28da611df30a391ab550f2cdb0a->enter($__internal_4e6496fa4cc2ffbd812649ef89b4643ead0cd28da611df30a391ab550f2cdb0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4e6496fa4cc2ffbd812649ef89b4643ead0cd28da611df30a391ab550f2cdb0a->leave($__internal_4e6496fa4cc2ffbd812649ef89b4643ead0cd28da611df30a391ab550f2cdb0a_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_50a497538bb554a9de1b1a19354b34d148ceb31c6ee5bcf0b77bd7a942f9e1bd = $this->env->getExtension("native_profiler");
        $__internal_50a497538bb554a9de1b1a19354b34d148ceb31c6ee5bcf0b77bd7a942f9e1bd->enter($__internal_50a497538bb554a9de1b1a19354b34d148ceb31c6ee5bcf0b77bd7a942f9e1bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_50a497538bb554a9de1b1a19354b34d148ceb31c6ee5bcf0b77bd7a942f9e1bd->leave($__internal_50a497538bb554a9de1b1a19354b34d148ceb31c6ee5bcf0b77bd7a942f9e1bd_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
