<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_a93e994eff55da4eaffd51f6c2ab943e8605b99d44ebec0384eda173f99f81a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d431dcfc047f37c264793e5b4eb65c6d6392fbf41a8c35e8f0bc628882f1b71 = $this->env->getExtension("native_profiler");
        $__internal_1d431dcfc047f37c264793e5b4eb65c6d6392fbf41a8c35e8f0bc628882f1b71->enter($__internal_1d431dcfc047f37c264793e5b4eb65c6d6392fbf41a8c35e8f0bc628882f1b71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_1d431dcfc047f37c264793e5b4eb65c6d6392fbf41a8c35e8f0bc628882f1b71->leave($__internal_1d431dcfc047f37c264793e5b4eb65c6d6392fbf41a8c35e8f0bc628882f1b71_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if (!$label) { $label = isset($label_format)*/
/*     ? strtr($label_format, array('%name%' => $name, '%id%' => $id))*/
/*     : $view['form']->humanize($name); } ?>*/
/* <button type="<?php echo isset($type) ? $view->escape($type) : 'button' ?>" <?php echo $view['form']->block($form, 'button_attributes') ?>><?php echo $view->escape(false !== $translation_domain ? $view['translator']->trans($label, array(), $translation_domain) : $label) ?></button>*/
/* */
