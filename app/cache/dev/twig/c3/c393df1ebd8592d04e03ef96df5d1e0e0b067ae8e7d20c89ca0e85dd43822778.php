<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_2ebe595384e672fb506b1f9148d568686f91585e24eb79e82ad87344c8a47dfc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fee2250680575ae8c253497e5323887fd4941678d87421d023f057489032d5f = $this->env->getExtension("native_profiler");
        $__internal_5fee2250680575ae8c253497e5323887fd4941678d87421d023f057489032d5f->enter($__internal_5fee2250680575ae8c253497e5323887fd4941678d87421d023f057489032d5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget',  array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_5fee2250680575ae8c253497e5323887fd4941678d87421d023f057489032d5f->leave($__internal_5fee2250680575ae8c253497e5323887fd4941678d87421d023f057489032d5f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'button_widget',  array('type' => isset($type) ? $type : 'reset')) ?>*/
/* */
