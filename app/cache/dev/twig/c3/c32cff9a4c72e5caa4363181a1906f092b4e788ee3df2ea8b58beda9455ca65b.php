<?php

/* @Twig/Exception/error.atom.twig */
class __TwigTemplate_b80d2fa0d7b676c5c849b257d2d09e7cf50a42808da4abd32d0a4910e079aa27 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65dcde76b24079506da5f83f15857aa35432b9e4bb27277a31f164fa91459e9f = $this->env->getExtension("native_profiler");
        $__internal_65dcde76b24079506da5f83f15857aa35432b9e4bb27277a31f164fa91459e9f->enter($__internal_65dcde76b24079506da5f83f15857aa35432b9e4bb27277a31f164fa91459e9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "@Twig/Exception/error.atom.twig", 1)->display($context);
        
        $__internal_65dcde76b24079506da5f83f15857aa35432b9e4bb27277a31f164fa91459e9f->leave($__internal_65dcde76b24079506da5f83f15857aa35432b9e4bb27277a31f164fa91459e9f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/error.xml.twig' %}*/
/* */
