<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_78c54eba2e77ca65b60adf393d366e669c4bfa69175742202493f96000b80cff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a1a6086ee2bb5c536d663a4ab8d64c6c4111a1426ba4599a426698fe064790f = $this->env->getExtension("native_profiler");
        $__internal_8a1a6086ee2bb5c536d663a4ab8d64c6c4111a1426ba4599a426698fe064790f->enter($__internal_8a1a6086ee2bb5c536d663a4ab8d64c6c4111a1426ba4599a426698fe064790f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_8a1a6086ee2bb5c536d663a4ab8d64c6c4111a1426ba4599a426698fe064790f->leave($__internal_8a1a6086ee2bb5c536d663a4ab8d64c6c4111a1426ba4599a426698fe064790f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td></td>*/
/*     <td>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
