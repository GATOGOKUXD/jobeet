<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_fb425b545a826cb82fb2a047d4a0e8071988f4da32a8455206ea69b05a0407ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dfa1c3272c466571fa9d2642a2f7094951adab7a52cb9b8f85bbc4d80f784a6d = $this->env->getExtension("native_profiler");
        $__internal_dfa1c3272c466571fa9d2642a2f7094951adab7a52cb9b8f85bbc4d80f784a6d->enter($__internal_dfa1c3272c466571fa9d2642a2f7094951adab7a52cb9b8f85bbc4d80f784a6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_dfa1c3272c466571fa9d2642a2f7094951adab7a52cb9b8f85bbc4d80f784a6d->leave($__internal_dfa1c3272c466571fa9d2642a2f7094951adab7a52cb9b8f85bbc4d80f784a6d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr style="display: none">*/
/*     <td colspan="2">*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
