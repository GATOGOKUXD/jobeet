<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_ddc5c53e48894e71d8ce31bbc90ca0010a2015719ddc9bfa48713d03c034b74d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46456a11dd989a8b8d5a0cbfc9092d37b510ce884f2f47b31c8a8636f68b645f = $this->env->getExtension("native_profiler");
        $__internal_46456a11dd989a8b8d5a0cbfc9092d37b510ce884f2f47b31c8a8636f68b645f->enter($__internal_46456a11dd989a8b8d5a0cbfc9092d37b510ce884f2f47b31c8a8636f68b645f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_46456a11dd989a8b8d5a0cbfc9092d37b510ce884f2f47b31c8a8636f68b645f->leave($__internal_46456a11dd989a8b8d5a0cbfc9092d37b510ce884f2f47b31c8a8636f68b645f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  28 => 3,  24 => 2,  22 => 1,);
    }
}
/* {% if exception.trace|length %}*/
/* {% for trace in exception.trace %}*/
/* {% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}*/
/* */
/* {% endfor %}*/
/* {% endif %}*/
/* */
