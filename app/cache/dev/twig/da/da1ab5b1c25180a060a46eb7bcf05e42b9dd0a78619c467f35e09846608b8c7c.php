<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_94bea4f8d08028fc1f78071e0604909c9a7805f6ea65e973b1cd58e88012d4a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d36294378bc365e425064de3988d2e5cf7608f26fcde16e23780a43c880858e = $this->env->getExtension("native_profiler");
        $__internal_2d36294378bc365e425064de3988d2e5cf7608f26fcde16e23780a43c880858e->enter($__internal_2d36294378bc365e425064de3988d2e5cf7608f26fcde16e23780a43c880858e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_2d36294378bc365e425064de3988d2e5cf7608f26fcde16e23780a43c880858e->leave($__internal_2d36294378bc365e425064de3988d2e5cf7608f26fcde16e23780a43c880858e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php foreach ($form as $child) : ?>*/
/*     <?php echo $view['form']->row($child) ?>*/
/* <?php endforeach; ?>*/
/* */
