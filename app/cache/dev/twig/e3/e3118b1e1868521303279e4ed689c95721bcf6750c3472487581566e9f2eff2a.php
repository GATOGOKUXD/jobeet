<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_389cf833fe53da03453374f702ba54d7edb73f683726bc601ca54600c4b183c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2ce387f646b0755ce9ba240971c2fd8c0f388be3965d65baa32dc00c2f85873 = $this->env->getExtension("native_profiler");
        $__internal_b2ce387f646b0755ce9ba240971c2fd8c0f388be3965d65baa32dc00c2f85873->enter($__internal_b2ce387f646b0755ce9ba240971c2fd8c0f388be3965d65baa32dc00c2f85873_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_b2ce387f646b0755ce9ba240971c2fd8c0f388be3965d65baa32dc00c2f85873->leave($__internal_b2ce387f646b0755ce9ba240971c2fd8c0f388be3965d65baa32dc00c2f85873_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->start($form) ?>*/
/*     <?php echo $view['form']->widget($form) ?>*/
/* <?php echo $view['form']->end($form) ?>*/
/* */
