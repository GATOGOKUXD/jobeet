<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_05fc892834201a06b0d70b94c75841096079d8cbb397850d99465f4a4a751676 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6d894a6ca4e8a1cd8d71398860d6659b8f682096c8c36f244807f9fc0faa57e = $this->env->getExtension("native_profiler");
        $__internal_a6d894a6ca4e8a1cd8d71398860d6659b8f682096c8c36f244807f9fc0faa57e->enter($__internal_a6d894a6ca4e8a1cd8d71398860d6659b8f682096c8c36f244807f9fc0faa57e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.atom.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_a6d894a6ca4e8a1cd8d71398860d6659b8f682096c8c36f244807f9fc0faa57e->leave($__internal_a6d894a6ca4e8a1cd8d71398860d6659b8f682096c8c36f244807f9fc0faa57e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
