<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_f2b57df042947b4400f1456b97585d5ef9c28a035c93366de9b8ec351d8dc805 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bdf4861fc68eaece47d57be1a70746a24f1f76eb992e5ea90c2a9149e44d9d2a = $this->env->getExtension("native_profiler");
        $__internal_bdf4861fc68eaece47d57be1a70746a24f1f76eb992e5ea90c2a9149e44d9d2a->enter($__internal_bdf4861fc68eaece47d57be1a70746a24f1f76eb992e5ea90c2a9149e44d9d2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple',  array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_bdf4861fc68eaece47d57be1a70746a24f1f76eb992e5ea90c2a9149e44d9d2a->leave($__internal_bdf4861fc68eaece47d57be1a70746a24f1f76eb992e5ea90c2a9149e44d9d2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php echo $view['form']->block($form, 'form_widget_simple',  array('type' => isset($type) ? $type : 'password')) ?>*/
/* */
