<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_012cb77466aed8faf1e307d9b693d7b8818c19850ca55cdb3391c29d6e0ba53c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f874b8e66ba3df0a67aeb9b92f1848f7c1777c692419d0ce1d5e03c2e5410728 = $this->env->getExtension("native_profiler");
        $__internal_f874b8e66ba3df0a67aeb9b92f1848f7c1777c692419d0ce1d5e03c2e5410728->enter($__internal_f874b8e66ba3df0a67aeb9b92f1848f7c1777c692419d0ce1d5e03c2e5410728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_f874b8e66ba3df0a67aeb9b92f1848f7c1777c692419d0ce1d5e03c2e5410728->leave($__internal_f874b8e66ba3df0a67aeb9b92f1848f7c1777c692419d0ce1d5e03c2e5410728_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <?php if ($widget == 'single_text'): ?>*/
/*     <?php echo $view['form']->block($form, 'form_widget_simple'); ?>*/
/* <?php else: ?>*/
/*     <div <?php echo $view['form']->block($form, 'widget_container_attributes') ?>>*/
/*         <?php echo $view['form']->widget($form['date']).' '.$view['form']->widget($form['time']) ?>*/
/*     </div>*/
/* <?php endif ?>*/
/* */
