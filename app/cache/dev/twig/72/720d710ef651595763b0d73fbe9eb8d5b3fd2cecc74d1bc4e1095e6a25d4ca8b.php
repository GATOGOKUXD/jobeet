<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_1b4a45c422c0db1ccfa730a6c1878a86dafbe49355d38793ee93bbd469fc9960 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd5363d6e5df44e3f8ba9ce223dcfca358cbce51ee016ef82518b8e68e339b58 = $this->env->getExtension("native_profiler");
        $__internal_fd5363d6e5df44e3f8ba9ce223dcfca358cbce51ee016ef82518b8e68e339b58->enter($__internal_fd5363d6e5df44e3f8ba9ce223dcfca358cbce51ee016ef82518b8e68e339b58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "@Twig/Exception/exception.rdf.twig", 1)->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")))));
        
        $__internal_fd5363d6e5df44e3f8ba9ce223dcfca358cbce51ee016ef82518b8e68e339b58->leave($__internal_fd5363d6e5df44e3f8ba9ce223dcfca358cbce51ee016ef82518b8e68e339b58_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}*/
/* */
