<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_089e7bc16edeb58e29fcfc2ae791aa408066e99ca76bdec01953e4dd707a9677 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_781499ab8b1a47d2daf8f53b63edfb11a439d38d38908df2aa4a19ef43114e70 = $this->env->getExtension("native_profiler");
        $__internal_781499ab8b1a47d2daf8f53b63edfb11a439d38d38908df2aa4a19ef43114e70->enter($__internal_781499ab8b1a47d2daf8f53b63edfb11a439d38d38908df2aa4a19ef43114e70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_781499ab8b1a47d2daf8f53b63edfb11a439d38d38908df2aa4a19ef43114e70->leave($__internal_781499ab8b1a47d2daf8f53b63edfb11a439d38d38908df2aa4a19ef43114e70_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_846a2dca6085dbcf011ad37d2ae09a943e83a9f2028f7f2ffde52d3302c771c2 = $this->env->getExtension("native_profiler");
        $__internal_846a2dca6085dbcf011ad37d2ae09a943e83a9f2028f7f2ffde52d3302c771c2->enter($__internal_846a2dca6085dbcf011ad37d2ae09a943e83a9f2028f7f2ffde52d3302c771c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_846a2dca6085dbcf011ad37d2ae09a943e83a9f2028f7f2ffde52d3302c771c2->leave($__internal_846a2dca6085dbcf011ad37d2ae09a943e83a9f2028f7f2ffde52d3302c771c2_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }
}
/* {% block panel '' %}*/
/* */
