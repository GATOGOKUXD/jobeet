<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_1b0a012462f86d8653e197f501f820d6f97542ab9c0fbe387e9201a092c8cb89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8111ef4a3dbefb41c4666e47478dbbf7748b53a75706d0e039021e874d289721 = $this->env->getExtension("native_profiler");
        $__internal_8111ef4a3dbefb41c4666e47478dbbf7748b53a75706d0e039021e874d289721->enter($__internal_8111ef4a3dbefb41c4666e47478dbbf7748b53a75706d0e039021e874d289721_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_8111ef4a3dbefb41c4666e47478dbbf7748b53a75706d0e039021e874d289721->leave($__internal_8111ef4a3dbefb41c4666e47478dbbf7748b53a75706d0e039021e874d289721_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <input type="<?php echo isset($type) ? $view->escape($type) : 'text' ?>" <?php echo $view['form']->block($form, 'widget_attributes') ?><?php if (!empty($value) || is_numeric($value)): ?> value="<?php echo $view->escape($value) ?>"<?php endif ?> />*/
/* */
