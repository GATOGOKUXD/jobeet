<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_770e9772c7077abfcc7d28ba3f2365a7d70977e8bce84f4d49dd384b93de6f4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e20826c2b8e5d8325db9c075144f8a3ef0b927b4252685b6091f6ac17d51409d = $this->env->getExtension("native_profiler");
        $__internal_e20826c2b8e5d8325db9c075144f8a3ef0b927b4252685b6091f6ac17d51409d->enter($__internal_e20826c2b8e5d8325db9c075144f8a3ef0b927b4252685b6091f6ac17d51409d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e20826c2b8e5d8325db9c075144f8a3ef0b927b4252685b6091f6ac17d51409d->leave($__internal_e20826c2b8e5d8325db9c075144f8a3ef0b927b4252685b6091f6ac17d51409d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* <tr>*/
/*     <td>*/
/*         <?php echo $view['form']->label($form) ?>*/
/*     </td>*/
/*     <td>*/
/*         <?php echo $view['form']->errors($form) ?>*/
/*         <?php echo $view['form']->widget($form) ?>*/
/*     </td>*/
/* </tr>*/
/* */
