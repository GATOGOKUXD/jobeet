<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_ef1ef7ee747d2abc35c0ffc61e3bfa4428096b4b1af92ea6a54b1fc59d0c0de5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78754aaec30ad62412bb7c47af966a241315ced76a1be43821187f31c64894ed = $this->env->getExtension("native_profiler");
        $__internal_78754aaec30ad62412bb7c47af966a241315ced76a1be43821187f31c64894ed->enter($__internal_78754aaec30ad62412bb7c47af966a241315ced76a1be43821187f31c64894ed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_78754aaec30ad62412bb7c47af966a241315ced76a1be43821187f31c64894ed->leave($__internal_78754aaec30ad62412bb7c47af966a241315ced76a1be43821187f31c64894ed_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  22 => 1,);
    }
}
/* Oops! An Error Occurred*/
/* =======================*/
/* */
/* The server returned a "{{ status_code }} {{ status_text }}".*/
/* */
/* Something is broken. Please let us know what you were doing when this error occurred.*/
/* We will fix it as soon as possible. Sorry for any inconvenience caused.*/
/* */
