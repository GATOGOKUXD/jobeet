<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_e28c1dfc79f50b8f5cb410efe0ca826604a4081f1866000d47b02c4214910f60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f458b4172b263014fa8c538dbf2574836c7778a692f6033f60060178cdf6957b = $this->env->getExtension("native_profiler");
        $__internal_f458b4172b263014fa8c538dbf2574836c7778a692f6033f60060178cdf6957b->enter($__internal_f458b4172b263014fa8c538dbf2574836c7778a692f6033f60060178cdf6957b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_f458b4172b263014fa8c538dbf2574836c7778a692f6033f60060178cdf6957b->leave($__internal_f458b4172b263014fa8c538dbf2574836c7778a692f6033f60060178cdf6957b_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }
}
/* {{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}*/
/* */
